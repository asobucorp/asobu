var bcrypt = require('bcrypt')
const User = require('../models').User;
const { check, validationResult } = require('express-validator/check');
const { matchedData } = require('express-validator/filter');

exports.register_form = function(req, res) {
    console.log("called")
    res.render('users/register_form')
}

exports.register_action = function(req,res,next) {
    req.checkBody('username', "Usernames must be a minimum of 3 characters.")
	.exists()
	.isLength({min:3})
	.usernameFree().withMessage("That username is taken.")
    req.checkBody('email', "Email addresses must be of a valid format")
	.exists()
	.isEmail()
    req.checkBody('password').exists()
    req.checkBody('password_confirmation', "Password confirmation must be the same as the password").exists().equals(req.body.password)
    req.sanitize('username').trim()
    req.sanitize('email').trim()

    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.render('users/register_form', { errors: result.array() });
	} else {
	    const u = matchedData(req)
	    console.log(u)
	    bcrypt.hash(req.body.password, 10, function(err, hash) {
    		User.create({username:req.body.username,
    			     email:req.body.email,
    			     password_digest:hash})
     		    .then(user => res.render('users/register_action', {username:req.body.username, email:req.body.email}))
    		    .catch(error => res.status(400).send(error));
    	    });
	}
    });
}

exports.view = function(req, res){
    User.create({username:req.params.id})
     	.then(user => res.render('users/view', {title:user.id, user:user}))
	 	.catch(error => res.status(400).send(error));
};
