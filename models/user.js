var bcrypt = require('bcrypt');
'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
	username: {
	    type: DataTypes.STRING,
	    allowNull: false,
	},
	email: {
	    type: DataTypes.STRING,
	    allowNull: false,
	},
	password_digest: {
	    type: DataTypes.STRING,
	    validate: {
		notEmpty: true
	    }
	},
    },
				{
				    freezeTableName: true,
				    //indexes: [{unique: true, fields: ['email']}],
				    instanceMethods: {
					authenticate: function(value) {
					    if (bcrypt.compareSync(value, this.password_digest))
						return this;
					    else
						return false;
					}
				    }
				}
			       );
    
    User.associate = function(models) {
	// associations can be defined here
    }

    
    /*var hasSecurePassword = function(user, options, callback) {
	bcrypt.hash(user.get('password'), 10, function(err, hash) {
	    if (err) return callback(err);
	    user.set('password_digest', hash);
	    return callback(null, options);
	});
    };
    
    User.beforeCreate(function(user, options, callback) {
	user.email = user.email.toLowerCase();
	if (user.password)
	    hasSecurePassword(user, options, callback);
	else
	    return callback(null, options);
    })
    User.beforeUpdate(function(user, options, callback) {
	user.email = user.email.toLowerCase();
	if (user.password)
	    hasSecurePassword(user, options, callback);
	else
	    return callback(null, options);
    })*/
    return User;
}

    //module.exports = User;
