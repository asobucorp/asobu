var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session')
var validator = require('express-validator')

//var index = require('./routes/site');
var user = require('./routes/users');

var app = express();

// set up session 
var sess = {
  secret: 'keyboard cat',
  cookie: {}
}
app.use(session(sess))


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// set up validation
const User = require('./models').User;
app.use(validator({
    customValidators: {
	usernameFree(username) {
	    return new Promise((resolve, reject) => {
		User.findOne({ where: {username: username} })
		    .then(user => { console.log(user); if (user==null) resolve(true); else reject(user); })
	    });
	},
	emailFree(email) {
	    return new Promise((resolve, reject) => {
		User.findOne({ where: {email: email} }, (err, user) => {
		    if (err) { console.log(err); throw err; }
		    if (user==null) {
			resolve();
		    } else {
			reject();
		    }
		});
	    });
	},
    }
}));

/*app.use('/', index);
  app.use('/users', users);*/

// Index routes
//app.get('/', site.index)

// User routes
//app.get('/users/:id', user.view);

app.get('/users/register',  user.register_form)
app.post('/users/register', user.register_action)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
